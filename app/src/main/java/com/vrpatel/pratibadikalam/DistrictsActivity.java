package com.vrpatel.pratibadikalam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class DistrictsActivity extends Fragment {

    BottomNavigationView bottomNavigationView;
    WebView webView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_districts, container, false);
//        setContentView(R.layout.activity_districts);




        webView = view.findViewById(R.id.districts_page_web_view);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("http://pratibadikalam.news/bn/district");

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        return view;
    }

//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            exitByBackKey();
//
//            //moveTaskToBack(false);
//
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//
//    protected void exitByBackKey() {
//
//        AlertDialog alertbox = new AlertDialog.Builder(this)
//                .setMessage("Do you want to exit application?")
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//
//                    // do something when the button is clicked
//                    public void onClick(DialogInterface arg0, int arg1) {
//
//                        finish();
//                        //close();
//
//
//                    }
//                })
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//
//                    // do something when the button is clicked
//                    public void onClick(DialogInterface arg0, int arg1) {
//                    }
//                })
//                .show();
//
//    }
}