package com.vrpatel.pratibadikalam;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

public class DownloadPDF extends AsyncTask<String, Void, Void> {

    private WeakReference<EpaperActivity> activityWeakReference;

    Context ctx;

    DownloadPDF(EpaperActivity activity) {
        activityWeakReference = new WeakReference<EpaperActivity>(activity);
    }

    @SuppressLint("LongLogTag")
    @Override
    protected Void doInBackground(String... strings) {

//        EpaperActivity activity = activityWeakReference.get();
//        if (activity == null || activity.isFinishing()) {
//            return null;
//        }

        //Creating the name of files and urls
        String fileUrl = strings[0];
        String fileName = strings[1];

        //Getting reference for external storage documents directory
        File folder = new File(Environment.getExternalStorageDirectory(), Environment.DIRECTORY_DOWNLOADS);

        if(folder.isDirectory() && folder.exists()){

        }
        else{
            Log.i("mkDir", "Download folder path: " + folder);
            Log.i("mkDir", "Download folder missing on the phone.");
            boolean createDocsFolder = folder.mkdirs();
            if(createDocsFolder){
                Log.i("mkDir", "Download folder created successfully.");
            }
            else{
                Log.i("mkDir", "Download folder could not be created.");
            }
            //Toast.makeText(activity, "Downloads folder missing on the phone.", Toast.LENGTH_SHORT).show();
        }
        //Getting reference for external storage documents + PB24 app directory
        File dir = new File( folder + java.io.File.separator, "PB24EPapers/");
        if(dir.exists() && dir.isDirectory()) {
            //nothing to do here when folder is already there
        }
        else{
            boolean mkDirRes = dir.mkdirs();
            if(mkDirRes){

            }
            else{
                Log.i("mkDir", "Path: " + dir);
                Log.i("mkDir", "PB24EPapers folder could not created under downloads. Path: " + dir);
                //Toast.makeText(activity, "PB24EPapers folder could not created under downloads.", Toast.LENGTH_SHORT).show();
            }
        }

        File epaper_file = new File(dir, fileName);
        if(epaper_file.exists()){
//            activity.ViewPDF();
        }
        else{
            File pdfFile = new File(dir, fileName);
            try{
                pdfFile.createNewFile();
            }
            catch (IOException e){
                e.printStackTrace();
            }

            //File Downloader class
            int dw_res = FileDowloader.downloadFile(fileUrl, pdfFile);

            if(dw_res == 1){
//                activity.ViewPDF();
            }
            else{
                Log.i("mkDir", "E-Paper downloading failed. Try again.");
                //Toast.makeText(activity, "E-Paper downloading failed. Try again.", Toast.LENGTH_SHORT).show();
            }
        }

        return null;
    }
}
