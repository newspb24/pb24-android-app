package com.vrpatel.pratibadikalam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.vrpatel.pratibadikalam.Constant.MyPREFERENCES;
import static com.vrpatel.pratibadikalam.Constant.RandId;
import static com.vrpatel.pratibadikalam.Constant.UserEmail;
import static com.vrpatel.pratibadikalam.Constant.UserFirstName;
import static com.vrpatel.pratibadikalam.Constant.UserId;
import static com.vrpatel.pratibadikalam.Constant.UserLastName;
import static com.vrpatel.pratibadikalam.Constant.UserPhoneNumber;
import static com.vrpatel.pratibadikalam.Constant.UserSubscribedEndDate;
import static com.vrpatel.pratibadikalam.Constant.UserSubscribedId;

public class LoginActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigatorView;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 100;

    EditText login_user_name;
    EditText login_user_password;

    SharedPreferences sharedpreferences;
    private static ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        //updateUI(account);

        // Set the dimensions of the sign-in button.
        SignInButton signInButton = findViewById(R.id.sign_in_button);
        Button btn_login = findViewById(R.id.btn_login);

        TextView textView = (TextView) signInButton.getChildAt(0);
        textView.setText("Sign In / Sign Up");

         login_user_name = findViewById(R.id.login_user_name);
         login_user_password = findViewById(R.id.login_user_password);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (login_user_name.getText().toString().isEmpty() && login_user_password.getText().toString().isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Please enter UserName or Password", Toast.LENGTH_SHORT).show();
                    return;
                }
//                if( getActivity() != null && !getActivity().isFinishing()) {
                try {
                    progress.show();
                } catch (Exception e) {
                }

//                }
                // calling a method to post the data and passing our name and job.
                postLoginData(login_user_name.getText().toString(), login_user_password.getText().toString());
            }
        });


        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        bottomNavigatorView = findViewById(R.id.bottom_navigator);
        bottomNavigatorView.setSelectedItemId(R.id.menu_home);

        bottomNavigatorView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_home:
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                    case R.id.home:
                        return true;

                    case R.id.menu_epaper:
                        startActivity(new Intent(getApplicationContext(), EpaperActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                    case R.id.menu_districts:
                        startActivity(new Intent(getApplicationContext(), DistrictsActivity.class));
                        overridePendingTransition(0,0);
                        return true;

                    case R.id.menu_profile:
                        startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });

        ImageView login_scr_back_btn = (ImageView) findViewById(R.id.login_scr_back_btn);
        login_scr_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progress = new ProgressDialog(LoginActivity.this);
            progress.setCanceledOnTouchOutside(false);
//            progress.setTitle(getString(R.string.loading_title));
            progress.setMessage("Please wait....!!!");
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
            System.out.println("account========>");
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            System.out.println("account========>"+account);

            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
            if (acct != null) {
                String personName = acct.getDisplayName();
                String personGivenName = acct.getGivenName();
                String personFamilyName = acct.getFamilyName();
                String personEmail = acct.getEmail();
                String personId = acct.getId();
                Uri personPhoto = acct.getPhotoUrl();


                Log.w("gAPI Data: ", "personName=" + personName);
                Log.w("gAPI Data: ", "personGivenName=" + personGivenName);
                Log.w("gAPI Data: ", "personFamilyName=" + personFamilyName);
                Log.w("gAPI Data: ", "personEmail=" + personEmail);
                Log.w("gAPI Data: ", "personId=" + personId);
                Log.w("gAPI Data: ", "personPhoto=" + personPhoto);

                postGoogleLoginData(personEmail, personId, personName);
            }



            // Signed in successfully, show authenticated UI.
//            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("gAPI Exception: ", "signInResult:failed code=" + e.getStatusCode());
            Log.w("gAPI Exception: ", "signInResult:failed code=" +e.getMessage());
//            Log.w("gAPI Exception: ", "signInResult:failed code=" );
            e.printStackTrace();
            //updateUI(null);
        }
    }

    private void postLoginData(String email, String password) {

        // url to post our data
        String url = "http://pratibadikalam.news/en/api/users/user_login";
//        loadingPB.setVisibility(View.VISIBLE);

        // creating a new variable for our request queue
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);

        // on below line we are calling a string
        // request method to post the data to our API
        // in this we are calling a post method.
        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // inside on response method we are
                // hiding our progress bar
                // and setting data to edit text as empty
//                loadingPB.setVisibility(View.GONE);
//                login_user_name.setText("");
//                login_user_password.setText("");
                progress.hide();

                // on below line we are displaying a success toast message.
//                Toast.makeText(LoginActivity.this, "Data added to API", Toast.LENGTH_SHORT).show();
                try {
                    // on below line we are passing our response
                    // to json object to extract data from it.
                    JSONObject respObj = new JSONObject(response);

                    System.out.println("respObj========>"+respObj);

                    if (respObj.getString("status")=="true")
                    {



//                        Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();

                        checkUserSubscription(email, respObj.getString("user_id"));


                    }
                    else{
                        if (respObj.has("message")) {
                            String msg = respObj.getString("message");
                            Constant constant = new Constant();
                            constant.showAlert(LoginActivity.this, msg);
                        }
                    }

                    // below are the strings which we
                    // extract from our json object.
//                    String email = respObj.getString("email");
//                    String password = respObj.getString("password");

                    // on below line we are setting this string s to our text view.
//                    responseTV.setText("Name : " + name + "\n" + "Job : " + job);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                progress.hide();
                Toast.makeText(LoginActivity.this, "Fail to get response = " + error, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // below line we are creating a map for
                // storing our values in key and value pair.
                Map<String, String> params = new HashMap<String, String>();

                // on below line we are passing our key
                // and value pair to our parameters.
                params.put("email", email);
                params.put("password", password);

                // at last we are
                // returning our params.
                return params;
            }
        };
        // below line is to make
        // a json object request.
        queue.add(request);
    }

    private void postGoogleLoginData(String email, String id, String name) {
        progress.show();
        // url to post our data
        String url = "http://pratibadikalam.news/en/api/users/user_social_login";
//        loadingPB.setVisibility(View.VISIBLE);

        // creating a new variable for our request queue
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);

        // on below line we are calling a string
        // request method to post the data to our API
        // in this we are calling a post method.
        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // inside on response method we are
                // hiding our progress bar
                // and setting data to edit text as empty
//                loadingPB.setVisibility(View.GONE);
//                login_user_name.setText("");
//                login_user_password.setText("");

                progress.hide();
                // on below line we are displaying a success toast message.
//                Toast.makeText(LoginActivity.this, "Data added to API", Toast.LENGTH_SHORT).show();
                try {
                    // on below line we are passing our response
                    // to json object to extract data from it.
                    JSONObject respObj = new JSONObject(response);

                    System.out.println("respObj========>"+respObj);

                    if (respObj.getString("status").equals("true"))
                    {
//                        SharedPreferences.Editor editor = sharedpreferences.edit();
//
//                        editor.putString(UserId, respObj.getString("user_id"));
//
//                        editor.commit();
//                        Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();

//                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
//                        finish();
                        checkUserSubscription(email, respObj.getString("user_id"));
                    }
                    else{
                        if (respObj.has("message")) {
                            String msg = respObj.getString("message");
                            Constant constant = new Constant();
                            constant.showAlert(LoginActivity.this, msg);
                        }
                    }

                    // below are the strings which we
                    // extract from our json object.
//                    String email = respObj.getString("email");
//                    String password = respObj.getString("password");

                    // on below line we are setting this string s to our text view.
//                    responseTV.setText("Name : " + name + "\n" + "Job : " + job);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                progress.hide();
                Toast.makeText(LoginActivity.this, "Fail to get response = " + error, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // below line we are creating a map for
                // storing our values in key and value pair.
                Map<String, String> params = new HashMap<String, String>();

                // on below line we are passing our key
                // and value pair to our parameters.
                params.put("email", email);
                params.put("oauth_provider", "Google");
                params.put("oauth_id", id);
                params.put("full_name", name);
                params.put("avatar_small", "");
                params.put("avatar_medium", "");
                params.put("avatar_large", "");


                // at last we are
                // returning our params.
                return params;
            }
        };
        // below line is to make
        // a json object request.
        queue.add(request);
    }

    private void checkUserSubscription(String email, String user_id){

        progress.show();
        // url to post our data
        String url = "http://pratibadikalam.news/en/api/users/check_user_subscription";

        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);


        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progress.hide();

                try {

                    JSONObject respObj = new JSONObject(response);

                    System.out.println("respObj========>"+respObj);

                    if (respObj.getString("status").equals("true"))
                    {
                        if(respObj.has("data")) {
                            JSONArray dataArr = respObj.getJSONArray("data");

                            JSONObject dataObj = (JSONObject) dataArr.get(0);

                            SharedPreferences.Editor editor = sharedpreferences.edit();

                            editor.putString(RandId, dataObj.getString("rand_id"));

                            editor.commit();

                            if (dataObj.has("subscribed_plan_id") && dataObj.has("subscription_end_date")) {

                                if ((dataObj.getInt("subscribed_plan_id") > 0)) {
                                    isSubcribeExpaired(dataObj.getString("subscription_end_date"),
                                            user_id,
                                            dataObj.getString("first_name"),
                                            dataObj.getString("last_name"),
                                            dataObj.getString("email"),
                                            dataObj.getString("phone"),
                                            dataObj.getString("subscribed_plan_id"),
                                            dataObj.getString("rand_id"));

                                }
                                else {
                                    Intent intent = new Intent(getApplicationContext(), SelectPlanActivity.class);
                                    intent.putExtra(UserId, user_id);
                                    intent.putExtra(UserEmail, email);
                                    intent.putExtra(UserFirstName, dataObj.getString("first_name"));
                                    intent.putExtra(UserLastName, dataObj.getString("last_name"));
                                    intent.putExtra(UserSubscribedId, dataObj.getString("subscribed_plan_id"));
                                    intent.putExtra(UserSubscribedEndDate, dataObj.getString("subscription_end_date"));
                                    intent.putExtra(UserPhoneNumber, dataObj.getString("phone"));
                                    intent.putExtra("RandId", dataObj.getString("rand_id"));
                                    startActivity(intent);
                                }
                            }
                        }


                    }
                    else{
                        if (respObj.has("message")) {
                            String msg = respObj.getString("message");
                            Constant constant = new Constant();
                            constant.showAlert(LoginActivity.this, msg);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                progress.hide();
                Toast.makeText(LoginActivity.this, "Fail to get response = " + error, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                params.put("email", email);


                return params;
            }
        };

        queue.add(request);
    }

    Boolean isSubcribeExpaired(String end_date, String user_id, String first_name,
                                            String last_name,
                                                    String email,
                                                    String phone,
                                                    String subscribed_plan_id,
                               String rand_id){
        // Get Current Date Time
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String getCurrentDateTime = sdf.format(c.getTime());

        Log.d("getCurrentDateTime",getCurrentDateTime);
// getCurrentDateTime: 05/23/2016 18:49 PM

        System.out.println("getCurrentDateTime.compareTo(end_date)======>"+getCurrentDateTime.compareTo(end_date));


        if (getCurrentDateTime.compareTo(end_date) < 0)
        {
            System.out.println("getCurrentDateTime.compareTo(end_date)======>"+getCurrentDateTime.compareTo(end_date));

            SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString(UserId, user_id);
            editor.putString(UserFirstName, first_name);
            editor.putString(UserLastName, last_name);
            editor.putString(UserEmail, email);
            editor.putString(UserSubscribedId, subscribed_plan_id);
            editor.putString(UserSubscribedEndDate, end_date);
            editor.putString(UserPhoneNumber, phone);

            editor.commit();


                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();


        }
        else
        {
            Log.d("Return","end date is older than getCurrentDateTime ");
            Intent intent = new Intent(getApplicationContext(), SelectPlanActivity.class);
            intent.putExtra(UserId, user_id);
            intent.putExtra(UserEmail, email);
            intent.putExtra(UserFirstName, first_name);
            intent.putExtra(UserLastName, last_name);
            intent.putExtra(UserSubscribedId, subscribed_plan_id);
            intent.putExtra(UserSubscribedEndDate, end_date);
            intent.putExtra(UserPhoneNumber, phone);
            intent.putExtra("RandId", rand_id);
            startActivity(intent);

        }

        return false;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if ( progress!=null && progress.isShowing() ){
            progress.cancel();
        }
    }
}