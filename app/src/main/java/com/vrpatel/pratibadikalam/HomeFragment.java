package com.vrpatel.pratibadikalam;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.fragment.app.Fragment;

import static com.vrpatel.pratibadikalam.Constant.MyPREFERENCES;
import static com.vrpatel.pratibadikalam.Constant.RandId;
import static com.vrpatel.pratibadikalam.Constant.UserId;

public class HomeFragment extends Fragment {

    WebView webView;

    SharedPreferences sharedpreferences;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         View view = inflater.inflate(R.layout.fragment_home, container, false);

        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


//        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);

        String rand_id = sharedpreferences.getString(RandId, "");

        String url = "http://pratibadikalam.news/bn/login/impersonate/"+rand_id;
        System.out.println("url for web========>"+url);
        webView = view.findViewById(R.id.home_page_web_view);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(url);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        return view;
    }

}