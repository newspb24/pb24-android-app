package com.vrpatel.pratibadikalam;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

public class Constant {

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String UserId = "user_id_key";
    public static final String UserFirstName = "first_name_key";
    public static final String UserLastName = "last_name_key";
    public static final String UserEmail = "user_email_key";
    public static final String UserSubscribedId = "subscribed_id_key";
    public static final String UserSubscribedEndDate = "subscribed_end_date_key";
    public static final String UserPhoneNumber = "user_phone_number";
    public static final String RandId = "rand_id_key";


    void showAlert(Context context, String msg){
        AlertDialog alertDialog = new AlertDialog.Builder(context)


                .setMessage(msg)

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //set what would happen when positive button is clicked
                        dialogInterface.dismiss();
                    }
                })

                .show();
    }


}
