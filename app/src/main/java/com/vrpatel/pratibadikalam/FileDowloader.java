package com.vrpatel.pratibadikalam;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class FileDowloader {
    private static final int MEGABYTE = 1024*1024;

    public static int downloadFile(String fileUrl, File directory){
        try{

            //URL Connection
            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();

            //Input Stream
            InputStream inputStream = urlConnection.getInputStream();

            //OutPut Stream
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            int totalSize = urlConnection.getContentLength();

            byte[] buffer = new byte[MEGABYTE];
            int bufferLength = 0;

            while((bufferLength = inputStream.read(buffer))>0){
                fileOutputStream.write(buffer, 0, bufferLength);
            }

            fileOutputStream.close();
            return 1;
        }catch (FileNotFoundException e){
            e.printStackTrace();
            Log.e("mkDir", "exception", e);
            return 0;
        }catch (MalformedURLException e){
            e.printStackTrace();
            Log.e("mkDir", "exception", e);
            return 0;
        }catch (IOException e){
            e.printStackTrace();
            Log.e("mkDir", "exception", e);
            return 0;
        }
    }
}
