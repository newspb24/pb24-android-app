package com.vrpatel.pratibadikalam.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.vrpatel.pratibadikalam.R;
import com.vrpatel.pratibadikalam.model.PlanModel;

import java.util.ArrayList;

public class PlanAdapter extends ArrayAdapter<PlanModel> {
    public PlanAdapter(@NonNull Context context, ArrayList<PlanModel> courseModelArrayList) {
        super(context, 0, courseModelArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listitemView = convertView;
        if (listitemView == null) {
            // Layout Inflater inflates each item to be displayed in GridView.
            listitemView = LayoutInflater.from(getContext()).inflate(R.layout.card_item, parent, false);
        }
        PlanModel planModel = getItem(position);

        TextView txt_plan_name = listitemView.findViewById(R.id.txt_plan_name);
        TextView txt_plan_price = listitemView.findViewById(R.id.txt_plan_price);
        txt_plan_name.setText(planModel.getPlan_name());
        txt_plan_price.setText(planModel.getPlan_price());
        return listitemView;
    }
}
