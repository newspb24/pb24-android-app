package com.vrpatel.pratibadikalam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.vrpatel.pratibadikalam.adapter.ViewPagerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.vrpatel.pratibadikalam.Constant.MyPREFERENCES;
import static com.vrpatel.pratibadikalam.Constant.RandId;
import static com.vrpatel.pratibadikalam.Constant.UserEmail;
import static com.vrpatel.pratibadikalam.Constant.UserFirstName;
import static com.vrpatel.pratibadikalam.Constant.UserId;
import static com.vrpatel.pratibadikalam.Constant.UserLastName;
import static com.vrpatel.pratibadikalam.Constant.UserPhoneNumber;
import static com.vrpatel.pratibadikalam.Constant.UserSubscribedEndDate;
import static com.vrpatel.pratibadikalam.Constant.UserSubscribedId;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    WebView webView;

    SharedPreferences sharedpreferences;
    private static ProgressDialog progress;

    private ViewPager viewPager;
    private TabLayout tabLayout;

    private HomeFragment homeFragment;
    private EpaperActivity epaperFragment;
    private DistrictsActivity districtsFragment;
    private ProfileActivity profileFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inAppUpdate();
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

//        viewPager = findViewById(R.id.viewPager);
//        tabLayout = findViewById(R.id.tabLayout);

        homeFragment = new HomeFragment();
        epaperFragment = new EpaperActivity();
        districtsFragment = new DistrictsActivity();
        profileFragment = new ProfileActivity();

        progress = new ProgressDialog(MainActivity.this);
        progress.setCanceledOnTouchOutside(false);
//            progress.setTitle(getString(R.string.loading_title));
        progress.setMessage("Please wait....!!!");

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


//        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        String user_id = sharedpreferences.getString(UserId, "");

        String rand_id = sharedpreferences.getString(RandId, "");
        String user_email = sharedpreferences.getString(UserEmail, "");

        System.out.println("user_id in main activity===========>"+user_id);

        if(user_id.equals(""))
        {
            startActivity(new Intent(getApplicationContext(), WelcomeActivity.class));
            finish();
        }
        else{
            checkUserSubscription(user_email, user_id);
        }



        String androidOS = Build.VERSION.RELEASE;

        Log.i("mkDir", String.valueOf(androidOS));

//        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), 0);
//        //add fragments and set the adapter
//        viewPagerAdapter.addFragment(homeFragment,"Home");
//        viewPagerAdapter.addFragment(epaperFragment, "Epaper");
//        viewPagerAdapter.addFragment(districtsFragment, "Districts");
//        viewPagerAdapter.addFragment(profileFragment, "Profile");
//        viewPager.setAdapter(viewPagerAdapter);



//        tabLayout.addTab(tabLayout.newTab().setText("Home"));
//        tabLayout.addTab(tabLayout.newTab().setText("Epaper"));
//        tabLayout.addTab(tabLayout.newTab().setText("Districts"));
//        tabLayout.addTab(tabLayout.newTab().setText("Profile"));
//
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//
//        tabLayout.getTabAt(0).setIcon(R.drawable.ic_menu_home);
//        tabLayout.getTabAt(1).setIcon(R.drawable.ic_menu_epaper);
//        tabLayout.getTabAt(2).setIcon(R.drawable.ic_menu_districts);
//        tabLayout.getTabAt(3).setIcon(R.drawable.ic_menu_profile);
//
//        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

//        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });

        bottomNavigationView = findViewById(R.id.bottom_navigator);
        bottomNavigationView.setSelectedItemId(R.id.menu_home);

//        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        // as soon as the application opens the first
        // fragment should be shown to the user
        // in this case it is algorithm fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                switch (item.getItemId()){
                    case R.id.menu_home:
                        selectedFragment = new HomeFragment();
                        break;
                    case R.id.menu_epaper:
                        selectedFragment = new EpaperActivity();
                        break;
                    case R.id.menu_districts:
                        selectedFragment = new DistrictsActivity();
                        break;
                    case R.id.menu_profile:
                        selectedFragment = new ProfileActivity();
                        break;



//                    case R.id.menu_home:
//                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
//                        overridePendingTransition(0,0);
//                        finish();
//                        return true;
//
//                    case R.id.home:
//                        return true;
//
//                    case R.id.menu_epaper:
//                        startActivity(new Intent(getApplicationContext(), EpaperActivity.class));
//                        overridePendingTransition(0,0);
//                        finish();
//                        return true;
//
//                    case R.id.menu_districts:
//                        startActivity(new Intent(getApplicationContext(), DistrictsActivity.class));
//                        overridePendingTransition(0,0);
//                        finish();
//                        return true;
//
//                    case R.id.menu_profile:
//                        startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
//                        overridePendingTransition(0,0);
//                        finish();
//                        return true;
                }

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, selectedFragment)
                        .commit();
                return true;


            }
        });


//        String url = "http://pratibadikalam.news/bn/login/impersonate/"+rand_id;
//        System.out.println("url for web========>"+url);
//        webView = findViewById(R.id.home_page_web_view);
//        webView.setWebViewClient(new WebViewClient());
//        webView.loadUrl(url);
//
//        WebSettings webSettings = webView.getSettings();
//        webSettings.setJavaScriptEnabled(true);
    }

    private void inAppUpdate() {
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(this);

// Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

// Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // This example applies an immediate update. To apply a flexible update
                    // instead, pass in AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                // Request the update.
                try {
                    appUpdateManager.startUpdateFlowForResult(
                            // Pass the intent that is returned by 'getAppUpdateInfo()'.
                            appUpdateInfo,
                            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                            AppUpdateType.IMMEDIATE,
                            // The current activity making the update request.
                            this,
                            // Include a request code to later monitor this update request.
                            11);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void checkUserSubscription(String email, String user_id){

        progress.show();
        // url to post our data
        String url = "http://pratibadikalam.news/en/api/users/check_user_subscription";

        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);


        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progress.hide();

                try {

                    JSONObject respObj = new JSONObject(response);

                    System.out.println("respObj========>"+respObj);

                    if (respObj.getString("status").equals("true"))
                    {
                        if(respObj.has("data")) {
                            JSONArray dataArr = respObj.getJSONArray("data");

                            JSONObject dataObj = (JSONObject) dataArr.get(0);

                            SharedPreferences.Editor editor = sharedpreferences.edit();

                            editor.putString(RandId, dataObj.getString("rand_id"));

                            editor.commit();

                            if (dataObj.has("subscribed_plan_id") && dataObj.has("subscription_end_date")) {

                                if ((dataObj.getInt("subscribed_plan_id") > 0)) {
                                    isSubcribeExpaired(dataObj.getString("subscription_end_date"),
                                            user_id,
                                            dataObj.getString("first_name"),
                                            dataObj.getString("last_name"),
                                            dataObj.getString("email"),
                                            dataObj.getString("phone"),
                                            dataObj.getString("subscribed_plan_id"),
                                            dataObj.getString("rand_id"));

                                }
                                else {
                                    Intent intent = new Intent(getApplicationContext(), SelectPlanActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.putExtra(UserId, user_id);
                                    intent.putExtra(UserEmail, email);
                                    intent.putExtra(UserFirstName, dataObj.getString("first_name"));
                                    intent.putExtra(UserLastName, dataObj.getString("last_name"));
                                    intent.putExtra(UserSubscribedId, dataObj.getString("subscribed_plan_id"));
                                    intent.putExtra(UserSubscribedEndDate, dataObj.getString("subscription_end_date"));
                                    intent.putExtra(UserPhoneNumber, dataObj.getString("phone"));
                                    intent.putExtra("RandId", dataObj.getString("rand_id"));
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        }


                    }
                    else{
                        if (respObj.has("message")) {
                            String msg = respObj.getString("message");
                            Constant constant = new Constant();
                            constant.showAlert(MainActivity.this, msg);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                progress.hide();
                Toast.makeText(MainActivity.this, "Fail to get response = " + error, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                params.put("email", email);


                return params;
            }
        };

        queue.add(request);
    }

    Boolean isSubcribeExpaired(String end_date, String user_id, String first_name,
                               String last_name,
                               String email,
                               String phone,
                               String subscribed_plan_id,
                               String rand_id){
        // Get Current Date Time
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String getCurrentDateTime = sdf.format(c.getTime());

        Log.d("getCurrentDateTime",getCurrentDateTime);
// getCurrentDateTime: 05/23/2016 18:49 PM

        System.out.println("getCurrentDateTime.compareTo(end_date)======>"+getCurrentDateTime.compareTo(end_date));


        if (getCurrentDateTime.compareTo(end_date) < 0)
        {
            System.out.println("getCurrentDateTime.compareTo(end_date)======>"+getCurrentDateTime.compareTo(end_date));
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(UserId, user_id);
            editor.putString(UserFirstName, first_name);
            editor.putString(UserLastName, last_name);
            editor.putString(UserEmail, email);
            editor.putString(UserSubscribedId, subscribed_plan_id);
            editor.putString(UserSubscribedEndDate, end_date);
            editor.putString(UserPhoneNumber, phone);
            editor.commit();
        }
        else
        {
            Log.d("Return","end date is older than getCurrentDateTime ");
            Intent intent = new Intent(getApplicationContext(), SelectPlanActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(UserId, user_id);
            intent.putExtra(UserEmail, email);
            intent.putExtra(UserFirstName, first_name);
            intent.putExtra(UserLastName, last_name);
            intent.putExtra(UserSubscribedId, subscribed_plan_id);
            intent.putExtra(UserSubscribedEndDate, end_date);
            intent.putExtra(UserPhoneNumber, phone);
            intent.putExtra("RandId", rand_id);
            startActivity(intent);
            finish();
        }

        return false;
    }


    @Override
    public void onBackPressed() {
        if(webView!=null)
        {
            if(webView.canGoBack()){
                webView.goBack();
            } else {
                exitByBackKey();
            }
        }else{
            exitByBackKey();
        }
    }



    protected void exitByBackKey() {

        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Do you want to exit application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();
                        //close();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }

}