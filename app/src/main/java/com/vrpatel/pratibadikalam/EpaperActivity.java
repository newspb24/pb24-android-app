package com.vrpatel.pratibadikalam;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Build.VERSION.SDK_INT;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import javax.net.ssl.HttpsURLConnection;

public class EpaperActivity extends DialogFragment {

    private static ProgressDialog progress;
    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;

    BottomNavigationView bottomNavigationView;
    PDFView pdfView;
    private int STORAGE_PERMISSION_CODE = 1;
    private TabLayout mTabLayout;

    String pdfurl = "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";


    Long tsLong = System.currentTimeMillis()/1000;
    String current_timestamp = tsLong.toString();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_epaper, container, false);


        pdfView = view.findViewById(R.id.epaper_viewer);
        mTabLayout = view.findViewById(R.id.tabs);

        dateView = (TextView) view.findViewById(R.id.epaper_date);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month+1, day);

//        bottomNavigationView = findViewById(R.id.bottom_navigator);
//        bottomNavigationView.setSelectedItemId(R.id.menu_epaper);
//
//        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                switch (item.getItemId()){
//                    case R.id.menu_home:
//                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
//                        overridePendingTransition(0,0);
//                        finish();
//                        return true;
//
//                    case R.id.home:
//                        return true;
//
//                    case R.id.menu_epaper:
//                        startActivity(new Intent(getApplicationContext(), EpaperActivity.class));
//                        overridePendingTransition(0,0);
//                        finish();
//                        return true;
//
//                    case R.id.menu_districts:
//                        startActivity(new Intent(getApplicationContext(), DistrictsActivity.class));
//                        overridePendingTransition(0,0);
//                        finish();
//                        return true;
//
//                    case R.id.menu_profile:
//                        startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
//                        overridePendingTransition(0,0);
//                        finish();
//                        return true;
//                }
//
//                return false;
//            }
//        });

        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // setCurrentItem as the tab position
//                viewPager.setCurrentItem(tab.getPosition());
                pdfView.jumpTo(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        setDynamicFragmentToTabLayout();

//        if(ContextCompat.checkSelfPermission(EpaperActivity.this,
//                READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
//            checkEpaperAvailability();
//        }
//        else {
//            requestStoragePermission();
//        }

        Button changeEpaperDateBtn = (Button) view.findViewById(R.id.changeEpaperDateBtn);
        changeEpaperDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeEpaperDate();
            }
        });

        return view;

    }

    // show all the tab using DynamicFragmnetAdapter
    private void setDynamicFragmentToTabLayout() {
        // here we have given 10 as the tab number
        // you can give any number here

        mTabLayout.removeAllTabs();

        System.out.println(" pdfView.getPageCount()=======>"+ pdfView.getPageCount());

        for (int i = 1; i <=  pdfView.getPageCount(); i++) {
            // set the tab name as "Page: " + i
            mTabLayout.addTab(mTabLayout.newTab().setText("Page: " + i));
        }
//        DynamicFragmentAdapter mDynamicFragmentAdapter = new DynamicFragmentAdapter(getSupportFragmentManager(), mTabLayout.getTabCount());
//
//        // set the adapter
//        viewPager.setAdapter(mDynamicFragmentAdapter);
//
//        // set the current item as 0 (when app opens for first time)
//        viewPager.setCurrentItem(0);
    }

    // create an async task class for loading pdf file from URL.
    class RetrivePDFfromUrl extends AsyncTask<String, Void, InputStream> {
        @Override
        protected InputStream doInBackground(String... strings) {
            // we are using inputstream
            // for getting out PDF.
            InputStream inputStream = null;
            try {
                URL url = new URL(strings[0]);
                // below is the step where we are
                // creating our connection.
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    // response is success.
                    // we are getting input stream from url
                    // and storing it in our variable.
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }

            } catch (IOException e) {
                // this is the method
                // to handle errors.
                e.printStackTrace();
                return null;
            }
            return inputStream;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            // after the execution of our async
            // task we are loading our pdf in our pdf view.
            pdfView.setMaxZoom(10F);
            pdfView.fromStream(inputStream)
                    .autoSpacing(true)
                    .pageSnap(true)
                    .pageFling(true)
                    .swipeHorizontal(true)
                    .enableAnnotationRendering(true)
            .onPageChange(new OnPageChangeListener() {
                @Override
                public void onPageChanged(int page, int pageCount) {
                    mTabLayout.getTabAt(page).select();
                }
            })
                    .onLoad(new OnLoadCompleteListener() {
                        @Override
                        public void loadComplete(int nbPages) {

                            Log.d("Epaper Activity", "loadComplete: " + nbPages);

                            Log.d("Epaper Activity", "pages count: " + pdfView.getPageCount());
                            setDynamicFragmentToTabLayout();

                            dismissLoadingDialog();
                        }
                    })
                    .load();

//            setDynamicFragmentToTabLayout();

        }
    }

    private void ShowPDFFromURL(String epaperFileName, Integer calDay, Integer calMonth, Integer calYear){
//        DownloadPDF task = new DownloadPDF(this);
        pdfurl = "http://pratibadikalam.news/public_html/upload/pdfs/" + epaperFileName;

        System.out.println("pdfurl============>"+pdfurl);
        new RetrivePDFfromUrl().execute(pdfurl);
    }

//    private void DownloadPDFFromURL(String epaperFileName, Integer calDay, Integer calMonth, Integer calYear){
//        DownloadPDF task = new DownloadPDF(this);
//        task.execute("http://pratibadikalam.news/public_html/upload/pdfs/" + epaperFileName, calDay + "_" + calMonth + "_" + calYear + "_pb24_news_epaper.pdf");
//    }
    private void checkEpaperAvailability(){
//        if(ContextCompat.checkSelfPermission(EpaperActivity.this,
//                READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
//            showLoadingDialog();
//        }
        showLoadingDialog();
        String calDate =  dateView.getText().toString();
        String[] calDateArr = calDate.split("\\.");

        Integer calDay, calMonth, calYear;
        calDay = Integer.valueOf(calDateArr[0]);
        calMonth = Integer.valueOf(calDateArr[1]);
        calYear = Integer.valueOf(calDateArr[2]);

//
//
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "http://pratibadikalam.news/bn/api/epaper/get_epaper_details?day=" + calDay + "&month=" + calMonth + "&year=" + calYear + "&auth_tkn=kjhdf876dw86324nldkle9d90dq09e2jkdjfh487yd9qwdlakfnsjgkehituyrowerwelkr34ihifjkh"
                , null
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    boolean epaperStatus = false;
                    epaperStatus = response.getBoolean("status");
                    TextView err_msg = (TextView) getView().findViewById(R.id.err_msg);
                    if (epaperStatus) {
//                        err_msg.setText("");
//                        err_msg.setVisibility(View.GONE);
//                        Log.i("api res", String.valueOf(pdfView.getVisibility()));

                        JSONArray epaperPages = new JSONArray(response.getString("epaper_pages"));
                        if(epaperPages.length()==1){
                            JSONObject epaperDetailsObj = (JSONObject) epaperPages.get(0);
                            String epaperFileName = "";
                            epaperFileName = epaperDetailsObj.getString("thumbnail");
                            ShowPDFFromURL(epaperFileName,calDay,calMonth,calYear);

                            err_msg.setVisibility(View.GONE);
                            pdfView.setVisibility(View.VISIBLE);
                            mTabLayout.setVisibility(View.VISIBLE);


                        }
                        else{
                            err_msg.setText("No Epaper found. Error Code: 101");
                            err_msg.setVisibility(View.VISIBLE);
                            pdfView.setVisibility(View.GONE);
                            mTabLayout.setVisibility(View.GONE);
                            Toast toast = Toast.makeText(getContext(),
                                    "Error: more than one page found.",
                                    Toast.LENGTH_SHORT);
                            toast.show();

                            dismissLoadingDialog();
                        }

                    } else {
                        err_msg.setText(response.getString("message"));
                        Toast toast = Toast.makeText(getContext(),
                                "Sorry, No epaper found for " + calDay + "." + calMonth + "." + calYear,
                                Toast.LENGTH_SHORT);
                        toast.show();
                        err_msg.setVisibility(View.VISIBLE);
                        pdfView.setVisibility(View.GONE);

                        mTabLayout.setVisibility(View.GONE);
                        dismissLoadingDialog();
                    }
                } catch (JSONException e) {
                    Log.i("api res", "This is error");
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("api res", "Something went wrong");
            }
        });
        requestQueue.add(jsonObjectRequest);
    }
//
//    public void ViewPDF(){
//        String calDate =  dateView.getText().toString();
//        String[] calDateArr = calDate.split("\\.");
//
//        Integer calDay, calMonth, calYear;
//        calDay = Integer.valueOf(calDateArr[0]);
//        calMonth = Integer.valueOf(calDateArr[1]);
//        calYear = Integer.valueOf(calDateArr[2]);
//
//        File folder = new File(Environment.getExternalStorageDirectory(), Environment.DIRECTORY_DOWNLOADS);
//        File pdfFile = new File(folder + "/PB24EPapers/" + calDay + "_" + calMonth + "_" + calYear + "_pb24_news_epaper.pdf");
//        Uri path = Uri.fromFile(pdfFile);
//
//        if(pdfView.isRecycled()){
//            pdfView.setMaxZoom(10F);
//            pdfView.fromFile(pdfFile).load();
//            Log.i("mkDir", "success" + pdfFile);
//        }
//        else{
//            pdfView.recycle();
//            pdfView.recycle();
//            pdfView.recycle();
//            pdfView.recycle();
//            pdfView.setMaxZoom(10F);
//            pdfView.fromFile(pdfFile).load();
//
//            Log.i("mkDir", "pdfView needs to be recycle");
//        }
//        dismissLoadingDialog();
//    }
//
//    private void requestStoragePermission(){
//         if(ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE)){
//             new AlertDialog.Builder(this)
//                     .setTitle("Permission needed")
//                     .setMessage("You denied write permission, Please allow permission to view the epaper.")
//                     .setCancelable(false)
//                     .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                         @Override
//                         public void onClick(DialogInterface dialogInterface, int i) {
//                             ActivityCompat.requestPermissions(EpaperActivity.this, new String[] {READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
//                         }
//                     })
//                     .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                         @Override
//                         public void onClick(DialogInterface dialogInterface, int i) {
//                             dialogInterface.dismiss();
//                             TextView err_msg = (TextView) findViewById(R.id.err_msg);
//                             err_msg.setVisibility(View.VISIBLE);
//                         }
//                     }).create().show();
//         }
//         else{
//             ActivityCompat.requestPermissions(this, new String[] {READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
//         }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        if(requestCode == STORAGE_PERMISSION_CODE){
//            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
////                checkEpaperAvailability();
//                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//            }
//            else{
//                //Toast.makeText(this, "Permission Denied xx", Toast.LENGTH_SHORT).show();
//                new AlertDialog.Builder(this)
//                        .setCancelable(false)
//                        .setTitle("Permission needed")
//                        .setMessage("You denied write permission, Please allow permission to view the epaper.")
//                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                ActivityCompat.requestPermissions(EpaperActivity.this, new String[] {READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
//                            }
//                        })
//                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                                TextView err_msg = (TextView) findViewById(R.id.err_msg);
//                                err_msg.setText("You need to give Files permissions to view the epaper. Thanks");
//                                err_msg.setVisibility(View.VISIBLE);
//                            }
//                        }).create().show();
//            }
//        }
//    }
//
    public void showLoadingDialog() {
        if (progress == null) {
            progress = new ProgressDialog(getContext());
            progress.setCanceledOnTouchOutside(true);
//            progress.setTitle(getString(R.string.loading_title));
            progress.setMessage(getString(R.string.loading_title));
        }
//        progress.show();
//        if (!((Activity) this).isFinishing()) {
            try {
                progress.show();
            } catch (WindowManager.BadTokenException e) {
                Log.e("WindowManagerBad ", e.toString());
            }
//        }
    }
//
    public void dismissLoadingDialog() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }
//
    private void changeEpaperDate(){
        DatePickerDialog dialog = new DatePickerDialog(getContext(), myDateListener, year, month, day);
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
         dialog.show();

//        showDialog();
    }
//
    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(day).append(".")
                .append(month).append(".").append(year));
        checkEpaperAvailability();
    }
//
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
//        if (id == 999) {
            DatePickerDialog dialog = new DatePickerDialog(getContext(), myDateListener, year, month, day);
            dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            return dialog;
            //return new DatePickerDialog(this,                    myDateListener, year, month, day);
//        }
//        return null;

//        Calendar c = Calendar.getInstance();
//        int cyear = c.get(Calendar.YEAR);
//        int cmonth = c.get(Calendar.MONTH);
//        int cday = c.get(Calendar.DAY_OF_MONTH);
//        switch (id) {
//            case 999:
//                //start changes...
//                DatePickerDialog dialog = new DatePickerDialog(this, myDateListener, cyear, cmonth, cday);
//                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//                return dialog;
//            //end changes...
//        }
//        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
        DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker arg0,
                                  int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
                // arg1 = year
                // arg2 = month
                // arg3 = day
                showDate(arg1, arg2+1, arg3);
            }
        };

//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            exitByBackKey();
//
//            //moveTaskToBack(false);
//
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }

//    protected void exitByBackKey() {
//
//        AlertDialog alertbox = new AlertDialog.Builder(this)
//                .setMessage("Do you want to exit application?")
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//
//                    // do something when the button is clicked
//                    public void onClick(DialogInterface arg0, int arg1) {
//
//                        finish();
//                        //close();
//
//
//                    }
//                })
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//
//                    // do something when the button is clicked
//                    public void onClick(DialogInterface arg0, int arg1) {
//                    }
//                })
//                .show();
//
//    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        dismissLoadingDialog();
//    }
}