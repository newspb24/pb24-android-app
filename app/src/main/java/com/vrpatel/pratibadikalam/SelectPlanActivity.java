package com.vrpatel.pratibadikalam;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultListener;
import com.razorpay.PaymentResultWithDataListener;
import com.vrpatel.pratibadikalam.adapter.PlanAdapter;
import com.vrpatel.pratibadikalam.model.PlanModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.vrpatel.pratibadikalam.Constant.MyPREFERENCES;
import static com.vrpatel.pratibadikalam.Constant.RandId;
import static com.vrpatel.pratibadikalam.Constant.UserEmail;
import static com.vrpatel.pratibadikalam.Constant.UserFirstName;
import static com.vrpatel.pratibadikalam.Constant.UserId;
import static com.vrpatel.pratibadikalam.Constant.UserLastName;
import static com.vrpatel.pratibadikalam.Constant.UserPhoneNumber;
import static com.vrpatel.pratibadikalam.Constant.UserSubscribedEndDate;
import static com.vrpatel.pratibadikalam.Constant.UserSubscribedId;

public class SelectPlanActivity extends AppCompatActivity implements PaymentResultWithDataListener {

    GridView planGV;
    Button btn_payment;
    int selected_subscribed_plan_id = 0;
    String selected_subscribed_plan_name = "";
    double selected_plan_price = 0.0f;
    String rand_id = "";

    String user_id = "";
    String user_email = "";
    String order_id = "";
    String payment_signature = "";
    
    String user_phone_number = "";
    String user_first_name = "";
    String user_last_name = "";

    SharedPreferences sharedpreferences;


    private static ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_plan);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        btn_payment = findViewById(R.id.btn_payment);
        planGV = findViewById(R.id.gv_select_plan);
        planGV.requestFocusFromTouch();
        planGV.setSelection(1);

        Bundle extras = getIntent().getExtras();
        if(extras != null)
        {
            user_id = extras.getString(UserId);
            user_email = extras.getString(UserEmail);
            user_phone_number = extras.getString(UserPhoneNumber);
            rand_id = extras.getString("RandId");


        }

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        progress = new ProgressDialog(SelectPlanActivity.this);
        progress.setCanceledOnTouchOutside(false);
//            progress.setTitle(getString(R.string.loading_title));
        progress.setMessage("Please wait....!!!");


        Checkout.preload(getApplicationContext());

        ArrayList<PlanModel> planModelArrayList = new ArrayList<PlanModel>();
        planModelArrayList.add(new PlanModel(1, "Monthly", getString(R.string.Rs)+"48"));
        planModelArrayList.add(new PlanModel(2, "Quarterly", getString(R.string.Rs)+"120"));
        planModelArrayList.add(new PlanModel(3, "Half yearly", getString(R.string.Rs)+"250"));
        planModelArrayList.add(new PlanModel(4,"Yearly", getString(R.string.Rs)+"520"));


        PlanAdapter adapter = new PlanAdapter(this, planModelArrayList);
        planGV.setAdapter(adapter);

        planGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


//                adapter.notifyDataSetChanged(); // call it here
//                if(planGV.isItemChecked(i)) {
//                    view = planGV.getChildAt(i);
//                    view.setBackgroundColor(Color.WHITE);
//                }else {
//                    view = planGV.getChildAt(i);
//                    view.setBackgroundColor(Color.BLACK); //the color code is the background color of GridView
//                }

//                view.setBackgroundColor(Color.parseColor("#814f00"));


                selected_subscribed_plan_id = planModelArrayList.get(i).getSubscribed_plan_id();
                selected_subscribed_plan_name = planModelArrayList.get(i).getPlan_name();
                selected_plan_price = Double.parseDouble(planModelArrayList.get(i).getPlan_price().replace(getString(R.string.Rs),""));

                System.out.println("selected_subscribed_plan_id=======>"+selected_subscribed_plan_id);

            }
        });

        btn_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("==========click on payment button=========");

//                Checkout checkout = new Checkout();
//                checkout.setKeyID("rzp_test_sI1eG8qkW0PSgh");

                if(selected_subscribed_plan_id>0){
                    createOrderUserSubscription();
//                    startPayment();
//                    saveUserSubscription("test");
                }
                else{
                    Toast.makeText(SelectPlanActivity.this, getString(R.string.select_your_plan), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public void startPayment() {
        /**
         * Instantiate Checkout
         */
        Checkout checkout = new Checkout();
        checkout.setKeyID("rzp_live_c0Ab9xMdcmy5Ry");
        //checkout.setKeyID("rzp_live_FLUxPmuEVOcawJ");


        /**
         * Set your logo here
         */
        checkout.setImage(R.mipmap.ic_app_icon);

        /**
         * Reference to current activity
         */
        final Activity activity = this;

        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try {
            JSONObject options = new JSONObject();

            options.put("name", selected_subscribed_plan_name);
           // options.put("description", "Reference No. #123456");
//            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
//            options.put("order_id", "order_DBJOWzybf0sJbb");//from response of step 3.
            options.put("order_id", order_id);
            options.put("theme.color", "#3399cc");
            options.put("currency", "INR");
            options.put("amount", selected_plan_price*100);//pass amount in currency subunits
            options.put("prefill.email", user_email);
            options.put("prefill.contact",user_phone_number);
            JSONObject retryObj = new JSONObject();
            retryObj.put("enabled", true);
            retryObj.put("max_count", 4);
            options.put("retry", retryObj);

            checkout.open(activity, options);

        } catch(Exception e) {
            Log.e("Payment", "Error in starting Razorpay Checkout", e);
        }
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID, PaymentData paymentData) {
        /**
         * Add your logic here for a successful payment response
         */
        System.out.println("Successful payment id : "+ razorpayPaymentID);

        saveUserSubscription(razorpayPaymentID,paymentData.getOrderId(),paymentData.getSignature());
    }



    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        /**
         * Add your logic here for a failed payment response
         */

        System.out.println("Failed and cause is : "+ s);
        Constant constant = new Constant();
        constant.showAlert(SelectPlanActivity.this, "কোনো প্রকার সমস্যার সম্মুখীন হলে দয়া করে Call অথবা Whatsapp করুন : 8598068608");

    }

    private void saveUserSubscription(String payment_id,String order_id,String singnature){

        progress.show();
        // url to post our data
        String url = "http://pratibadikalam.news/en/api/users/save_user_subscription_plan";

        RequestQueue queue = Volley.newRequestQueue(SelectPlanActivity.this);


        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progress.hide();

                try {

                    JSONObject respObj = new JSONObject(response);

                    System.out.println("respObj========>"+respObj);

                    if (respObj.getString("status").equals("true"))
                    {

                        SharedPreferences.Editor editor = sharedpreferences.edit();

                        editor.putString(UserId, user_id);
                        editor.putString(UserEmail, user_email);
                        editor.commit();

                        Intent intent = new Intent(SelectPlanActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();



                    }
                    else{
                        if (respObj.has("message")) {
                            String msg = respObj.getString("message");
                            Constant constant = new Constant();
                            constant.showAlert(SelectPlanActivity.this, msg);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                progress.hide();
                Toast.makeText(SelectPlanActivity.this, "Fail to get response = " + error, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                params.put("email", user_email);
                params.put("plan_id", ""+selected_subscribed_plan_id);
                params.put("auth_tkn", "kjhdf876dw86324nldkle9d90dq09e2jkdjfh487yd9qwdlakfnsjgkehituyrowerwelkr34ihifjkh");
                params.put("subscription_payment_id", payment_id);
                params.put("rand_id", rand_id);
                params.put("payment_id", payment_id);
                params.put("order_id", order_id);
                params.put("singnature", singnature);
                System.out.println("respObj========>"+params);


//                params.put("email", "ashoksamdari@gmail.com");
//                params.put("plan_id", "1");
//                params.put("auth_tkn", "kjhdf876dw86324nldkle9d90dq09e2jkdjfh487yd9qwdlakfnsjgkehituyrowerwelkr34ihifjkh");
//                params.put("subscription_payment_id", "payment_id");
//                params.put("rand_id", "jb6Pr8");


                return params;
            }
        };

        queue.add(request);
    }
    private void createOrderUserSubscription(){

        progress.show();
        // url to post our data
        String url = "http://pratibadikalam.news/en/api/users/create_order_user_subscription_plan";

        RequestQueue queue = Volley.newRequestQueue(SelectPlanActivity.this);


        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progress.hide();

                try {

                    JSONObject respObj = new JSONObject(response);

                    System.out.println("respObj========>"+respObj);

                    if (respObj.getString("status").equals("true"))
                    {
                         order_id=respObj.getString("data");
                         startPayment();
//                        SharedPreferences.Editor editor = sharedpreferences.edit();
//
//                        editor.putString(UserId, user_id);
//                        editor.putString(UserEmail, user_email);
//                        editor.commit();

//                        Intent intent = new Intent(SelectPlanActivity.this, MainActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intent);
//                        finish();



                    }
                    else{
                        if (respObj.has("message")) {
                            String msg = respObj.getString("message");
                            Constant constant = new Constant();
                            constant.showAlert(SelectPlanActivity.this, msg);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                progress.hide();
                Toast.makeText(SelectPlanActivity.this, "Fail to get response = " + error, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                params.put("email", user_email);
                params.put("plan_id", ""+selected_subscribed_plan_id);
                params.put("auth_tkn", "kjhdf876dw86324nldkle9d90dq09e2jkdjfh487yd9qwdlakfnsjgkehituyrowerwelkr34ihifjkh");
                params.put("rand_id", rand_id);
                System.out.println("respObj========>"+params);
//                params.put("email", "ashoksamdari@gmail.com");
//                params.put("plan_id", "1");
//                params.put("auth_tkn", "kjhdf876dw86324nldkle9d90dq09e2jkdjfh487yd9qwdlakfnsjgkehituyrowerwelkr34ihifjkh");
//                params.put("subscription_payment_id", "payment_id");
//                params.put("rand_id", "jb6Pr8");


                return params;
            }
        };

        queue.add(request);
    }
}















