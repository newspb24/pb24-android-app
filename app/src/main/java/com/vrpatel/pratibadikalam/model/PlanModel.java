package com.vrpatel.pratibadikalam.model;

public class PlanModel {
    private int subscribed_plan_id;
    private String plan_name;

    private String plan_price;

    public PlanModel(int subscribed_plan_id, String plan_name, String plan_price) {
        this.subscribed_plan_id = subscribed_plan_id;
        this.plan_name = plan_name;
        this.plan_price = plan_price;
    }

    public int getSubscribed_plan_id() {
        return subscribed_plan_id;
    }

    public void setSubscribed_plan_id(int subscribed_plan_id) {
        this.subscribed_plan_id = subscribed_plan_id;
    }

    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }

    public String getPlan_price() {
        return plan_price;
    }

    public void setPlan_price(String plan_price) {
        this.plan_price = plan_price;
    }


}
