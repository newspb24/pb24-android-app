package com.vrpatel.pratibadikalam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.vrpatel.pratibadikalam.Constant.MyPREFERENCES;
import static com.vrpatel.pratibadikalam.Constant.RandId;
import static com.vrpatel.pratibadikalam.Constant.UserEmail;
import static com.vrpatel.pratibadikalam.Constant.UserFirstName;
import static com.vrpatel.pratibadikalam.Constant.UserId;
import static com.vrpatel.pratibadikalam.Constant.UserLastName;
import static com.vrpatel.pratibadikalam.Constant.UserPhoneNumber;
import static com.vrpatel.pratibadikalam.Constant.UserSubscribedEndDate;
import static com.vrpatel.pratibadikalam.Constant.UserSubscribedId;

public class ProfileActivity extends Fragment {

    BottomNavigationView bottomNavigationView;
    TextView txt_name;
    TextView txt_email;
    TextView txt_subscribe_plan;
    TextView txt_subscribe_end_date;
    TextView txt_mobile;
    Button btn_logout;

    GoogleSignInClient mGoogleSignInClient;

    private static ProgressDialog progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_profile, container, false);
//        setContentView(R.layout.activity_profile);

//        ActionBar actionBar = getSupportActionBar();
//        actionBar.hide();

        progress = new ProgressDialog(getContext());
        progress.setCanceledOnTouchOutside(false);
//            progress.setTitle(getString(R.string.loading_title));
        progress.setMessage("Please wait....!!!");

        SharedPreferences prefs = getActivity().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        String user_id = prefs.getString(UserId, "");
        String first_name = prefs.getString(UserFirstName, "");
        String last_name = prefs.getString(UserLastName, "");
        String email = prefs.getString(UserEmail, "");
        String subscribed_id = prefs.getString(UserSubscribedId, "");
        String subscribed_end_date = prefs.getString(UserSubscribedEndDate, "");
        String mobile_number = prefs.getString(UserPhoneNumber, "");

        bottomNavigationView = view.findViewById(R.id.bottom_navigator);
        txt_name = view.findViewById(R.id.txt_user_name);
        txt_email = view.findViewById(R.id.txt_user_email);
        txt_subscribe_plan = view.findViewById(R.id.txt_subscribed_plan);
        txt_subscribe_end_date = view.findViewById(R.id.txt_subscribed_end_date);
        txt_mobile = view.findViewById(R.id.txt_mobile_number);
        btn_logout = view.findViewById(R.id.btn_logout);

        checkUserSubscription(email, user_id);


        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               showLogoutAlert();
            }
        });

        return view;

    }

    private void checkUserSubscription(String email, String user_id){

        progress.show();
        // url to post our data
        String url = "http://pratibadikalam.news/en/api/users/check_user_subscription";

        RequestQueue queue = Volley.newRequestQueue(getContext());


        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progress.hide();

                try {

                    JSONObject respObj = new JSONObject(response);

                    System.out.println("respObj========>"+respObj);

                    if (respObj.getString("status").equals("true"))
                    {
                        if(respObj.has("data")) {
                            JSONArray dataArr = respObj.getJSONArray("data");

                            JSONObject dataObj = (JSONObject) dataArr.get(0);


                            txt_name.setText(dataObj.getString("first_name") + " " );
                            txt_email.setText(email);

                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            String subscribed_end_date =  dataObj.getString("subscription_end_date");
                            if (subscribed_end_date != null) {
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                                DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");

                                Date date = null;
                                try {
                                    date = inputFormat.parse(subscribed_end_date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String outputDateStr = outputFormat.format(date);


//            String end_date_format = sdf.format(subscribed_end_date);
                                txt_subscribe_end_date.setText(outputDateStr);
                            }
                            if (dataObj.getString("phone").equals("null"))
                            {
                                txt_mobile.setText("N/A");
                            }
                            else {
                                txt_mobile.setText(dataObj.getString("phone"));
                            }

                            switch (dataObj.getString("subscribed_plan_id")) {
                                case "1":
                                    txt_subscribe_plan.setText(R.string.monthly);
                                    break;
                                case "2":
                                    txt_subscribe_plan.setText(R.string.quarterly);
                                    break;
                                case "3":
                                    txt_subscribe_plan.setText(R.string.half_yearly);
                                    break;
                                case "4":
                                    txt_subscribe_plan.setText(R.string.yearly);
                                    break;
                            }




                        }


                    }
                    else{
                        if (respObj.has("message")) {
                            String msg = respObj.getString("message");
                            Constant constant = new Constant();
                            constant.showAlert(getContext(), msg);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                progress.hide();
                Toast.makeText(getContext(), "Fail to get response = " + error, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                params.put("email", email);


                return params;
            }
        };

        queue.add(request);
    }

    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(loginIntent);
                    }
                });
    }

    void showLogoutAlert(){
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())


                .setTitle(R.string.logout)
                .setMessage(R.string.logout_msg)

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //set what would happen when positive button is clicked

                        SharedPreferences preferences =getContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();

                        if(progress.isShowing())
                        {
                            progress.hide();
                        }

                        Intent loginIntent = new Intent(getContext(), LoginActivity.class);
                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(loginIntent);
                        getActivity().finish();

                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })

                .show();
    }

//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            exitByBackKey();
//
//            //moveTaskToBack(false);
//
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }

//    protected void exitByBackKey() {
//
//        AlertDialog alertbox = new AlertDialog.Builder(this)
//                .setMessage("Do you want to exit application?")
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//
//                    // do something when the button is clicked
//                    public void onClick(DialogInterface arg0, int arg1) {
//
//                        if(progress.isShowing())
//                        {
//                            progress.hide();
//                        }
//
//                        finish();
//                        //close();
//
//
//                    }
//                })
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//
//                    // do something when the button is clicked
//                    public void onClick(DialogInterface arg0, int arg1) {
//                    }
//                })
//                .show();
//
//    }
}